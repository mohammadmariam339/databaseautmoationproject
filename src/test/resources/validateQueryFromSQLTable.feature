Feature: Validating Database project

  @db
  Scenario Outline: Validating DB column
    Given User is able to connect to database
    When User sends the "<query>" to database
    Then validate information taken from the database displayed correct with the order
      | Jason    | Mallin     |
      | Michael  | Rogers     |
      | Ki       | Gee        |
      | Hazel    | Philtanker |
      | Kelly    | Chung      |
      | Jennifer | Dilly      |
      | Timothy  | Gates      |
      | Randall  | Perkins    |

    Examples: Query for the database
      | query                                                                                                                         |
      | SELECT first_name, last_name FROM employees WHERE manager_id = (SELECT employee_id FROM employees WHERE first_name = 'Payam') |