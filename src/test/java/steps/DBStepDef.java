package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import util.ConfigReader;
import util.DBUtil;

import java.util.List;

public class DBStepDef {

    private final Logger logger = LogManager.getLogger(DBStepDef.class);

    @Given("User is able to connect to database")
    public void user_is_able_to_connect_to_database() {
        DBUtil.createDBConnection();
    }

    @When("User sends the {string} to database")
    public void user_sends_the_to_database(String query) {
        DBUtil.getQueryResultList(query);
    }

    @Then("validate information taken from the database displayed correct with the order")
    public void validateInformationTakenFromTheDatabased(DataTable dataTable) {
        List<List<Object>> actualQuery = DBUtil.getQueryResultList(ConfigReader.getProperty("query"));
        List<List<String>> expectedQuery = dataTable.asLists();

        for (int i = 0; i < actualQuery.size(); i++) {
            for (int j = 0; j < actualQuery.get(i).size(); j++) {
                Assert.assertEquals(expectedQuery.get(i).get(j), actualQuery.get(i).get(j));
                logger.info(expectedQuery.get(i).get(j), actualQuery.get(i).get(j));
            }
        }
    }
}